const productReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return {
                name = action.name,
                price = action.price,
                types = action.types
            }

        case 'EDIT_PRODUCT':
            return {
                ...state,
                name = action.name,
                price = action.price
            }
        default:
            return state
    }
}

const productsReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state, userReducer(null, action)]

        case 'EDIT_PRODUCT':
            return state.map((each, index) => {
                if (each.name === action.name) {
                    return userReducer(each, action)
                }
                return each
            })
        default:
            return state
    }
}

